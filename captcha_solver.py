from LPRNet import build_lprnet
import numpy as np
import torch
import cv2


class NNCaptchaSolver:
    def __init__(self, img_size=(94, 24), on_gpu=True):
        self.net = build_lprnet(lpr_max_len=6, phase=False, class_num=11, dropout_rate=0.5)
        self.on_gpu = on_gpu

        if self.on_gpu:
            self.net = self.net.cuda()
            self.net.load_state_dict(torch.load("./model.pt"))
        else:
            self.net.load_state_dict(torch.load("./model.pt", map_location="cuda:0"))
        # self.net.train()
        self.img_size = img_size

    def convert(self, prebs):
        prebs = prebs.cpu().detach().numpy()
        preb_labels = list()
        for i in range(prebs.shape[0]):
            preb = prebs[i, :, :]
            preb_label = list()
            for j in range(preb.shape[1]):
                preb_label.append(np.argmax(preb[:, j], axis=0))
            no_repeat_blank_label = list()
            pre_c = preb_label[0]
            for num, c in enumerate(preb_label):  # dropout repeate label and blank label
                if (num != 0 and pre_c == c) or (c == 10):
                    # if (pre_c == c) or (c == 22):
                    if c == 10:
                        pre_c = c
                    continue
                no_repeat_blank_label.append(c)
                pre_c = c
            preb_labels.append(no_repeat_blank_label)
        return preb_labels

    def prep_image(self, img):
        img = img.astype("float32")
        resized_image = cv2.resize(img, self.img_size)
        resized_image = np.transpose(resized_image, (2, 0, 1))
        resized_image = resized_image.astype("float32")
        resized_image -= 127.5
        resized_image *= 0.0078125
        return resized_image

    def get_text(self, image_path):
        img = cv2.imread(image_path)
        prep_img = self.prep_image(img)

        inputs = torch.tensor([prep_img])
        if self.on_gpu:
            inputs = inputs.cuda()

        logits = self.net(inputs)
        res = self.convert(logits)
        return "".join(list(map(str, res[0])))

