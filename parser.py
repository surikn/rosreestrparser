from python3_anticaptcha import ImageToTextTask
from datetime import datetime
from bs4 import BeautifulSoup
from captcha_solver import NNCaptchaSolver
import requests
import json
import uuid
import time
import sys
import os


class RosReestrParser:
    # URL_BASE = "https://rosreestr.ru/wps/portal/"
    FORM_URL = "https://rosreestr.ru/wps/portal/online_request"
    IMAGE_URL = "https://rosreestr.ru/wps/portal/p/cc_ib_portal_services/online_request/!ut/p/z1/04_Sj9CPykssy0xPLMnMz0vMAfIjo8zi3QNNXA2dTQy93QMNzQ0cPR29DY0N3Q0MQkz1w_Eq8DfUj6JEP1ABSL8BDuBoANQfhdcKZyMCCkBOJGRJQW5ohEGmpyIAKLXudw!!/{}"
    SEND_FORM_URL = "https://rosreestr.ru/wps/portal/p/cc_ib_portal_services/online_request/!ut/p/z1/04_Sj9CPykssy0xPLMnMz0vMAfIjo8zi3QNNXA2dTQy93QMNzQ0cPR29DY0N3Q0MQkz1w_Eq8DfUj6JEP1ABSL8BDuBoANQfhdcKZyMCCkBOJGRJQW5ohEGmpyIAKLXudw!!/p0/IZ7_01HA1A42KODT90AR30VLN22001=CZ6_GQ4E1C41KGQ170AIAK131G00T5=MEcontroller!QCPSearchAction==/"
    FULL_PAGE_URL = "https://rosreestr.ru/wps/portal/p/cc_ib_portal_services/online_request/!ut/p/z1/pZBBU8IwEIV_jeckLbXCLQKDDgoCRWkvmTTstGHatBMSR_-9CS2jHoSDmVx299v3XoIytEOZ4u-y4EY2ileuTrNbNlsNpmQ8IPPZisSYPtI5CckM4yRCbxeBJUHZf_Yd4PfxH4dit59dtBgHVwAf8ZpJ6kLGDJMHSuggmC8nyRDTdYhfnxZBgDFBG68hGmV0U1WgUXqDN8C1KKnwH-mnx1PNzGcLKB3TCVtsn--naz_Kraz2UhXOx8vwPVO2Rmkcj3A0cvLOIhjFYYj9WEPhFU9oU_XoDwPeOaaJtnBq2_wAwnRM2dgj9Ljh2rC2OcqOj4ZdVwOYc5DWiJIn8OEaURQOOwKMqaAG1VNNfvjOAErYOtdcCWAu6NnJa_Yvd0X3DFmU5hdjhbG6D8dbF673aOvtDsuXur4L_7xfmuBNGg!!/{}"
    ANTICAPTCHA_KEY = "81eb28e43b5bc68a6ad992b8a48a439b"


    def __init__(self, timeout=5, captcha_solver=2, captcha_folder="captchas", result_folder="result", print_log=True, custom_headers=None, solver_gpu=True):
        self.TIMEOUT = timeout
        self.CAPTCHA_SOLVER = captcha_solver
        self.CAPTCHA_FOLDER = captcha_folder
        self.PRINT_LOG = print_log

        self.NN_CAPTCHA_SOLVER = NNCaptchaSolver(on_gpu=solver_gpu)

        self.RESULT_FOLDER = result_folder

        if custom_headers is None:
            self.HEADERS = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.95 Safari/537.36'}
        else:
            self.HEADERS = custom_headers

        self.SESSION = requests.Session()

        if not os.path.exists(self.CAPTCHA_FOLDER):
            os.makedirs(self.CAPTCHA_FOLDER)
        if not os.path.exists(self.RESULT_FOLDER):
            os.makedirs(self.RESULT_FOLDER)

    def log(self, text):
        if self.PRINT_LOG:
            print("LOG: ", text)

    def drop_session(self):
        self.SESSION = requests.Session()

    def change_headers(self, headers):
        self.HEADERS = headers

    def save_image(self, pic_url, filename=None):
        if filename is None:
            filename = str(uuid.uuid4()) + ".jpg"
        path = f"{self.CAPTCHA_FOLDER}/{filename}"

        self.log("Downloading captcha image")
        try:
            resp = self.SESSION.get(pic_url, timeout=self.TIMEOUT, stream=True, headers=self.HEADERS)
        except requests.exceptions.Timeout as ex:
            return {"ok": False, "error": "Timeout"}
        if resp.status_code != 200:
            return {"ok": False, "error": f"Server responsed {resp.status_code}"}

        self.log("Saving image...")
        with open(path, 'wb') as handle:
            for block in resp.iter_content(1024):
                if not block:
                    break
                handle.write(block)

        if os.path.getsize(path) == 0:
            os.remove(path)
            return {"ok": False, "error": "Invalid captcha image recieved"}

        return {"ok": True, "path": path, "filename": filename}

    def anti_captcha(self, image_link):
        self.log("Sending image to the anticaptcha...")
        user_answer = ImageToTextTask.ImageToTextTask(anticaptcha_key=self.ANTICAPTCHA_KEY).\
            captcha_handler(captcha_link=image_link)
        return user_answer

    def get_form_page(self):
        self.log("Requesting form page")
        try:
            resp = self.SESSION.get(self.FORM_URL, timeout=self.TIMEOUT, headers=self.HEADERS)
        except requests.exceptions.Timeout as ex:
            return {"ok": False, "error": "Timeout"}

        if resp.status_code != 200:
            return {"ok": False, "error": f"Server responsed {resp.status_code}"}

        form_html_doc = resp.text
        form_soup = BeautifulSoup(form_html_doc, "html.parser")
        captcha_image = form_soup.find(id="captchaImage2")

        if captcha_image is None:
            return {"ok": False, "error": "Invalid page recieved"}

        image_src = self.IMAGE_URL.format(captcha_image["src"])
        res = self.save_image(image_src)
        if not res["ok"]:
            return res

        if self.CAPTCHA_SOLVER == 2:
            # anticap_answer = self.anti_captcha(image_src)
            # print(anticap_answer)
            # if anticap_answer is not None and anticap_answer["errorId"] == 0:
            #     captcha_text = anticap_answer["solution"]["text"]
            #     self.log("Recieved code from anticaptcha")
            #     print(type(captcha_text), captcha_text)
            captcha_text = self.NN_CAPTCHA_SOLVER.get_text(res["path"])
            self.log(res["path"] + " " + captcha_text)
        else:
            self.log(f"""IMAGE PATH: {res["path"]} """)
            print("ENTER CAPTCHA CODE:", sep=" ")
            captcha_text = input()

        open(f"""{self.CAPTCHA_FOLDER}/{res["filename"]}.txt""", "w").write(captcha_text)

        return {"ok": True, "captcha_text": captcha_text}

    def send_form(self, cad_num, captcha_text):
        post_data = {
            "search_action": True,
            "subject": "",
            "region": "",
            "settlement": "",
            "search_type": "CAD_NUMBER",
            "cad_num": cad_num,
            "start_position": 59,
            "obj_num": "",
            "old_num": "",
            "street_type": "str0",
            "street": "",
            "house": "",
            "building": "",
            "structure": "",
            "apartment": "",
            "right_reg": "",
            "encumbrance_reg": "",
            "captchaText": captcha_text,
        }
        self.log("Sending form...")
        try:
            resp = self.SESSION.post(self.SEND_FORM_URL, data=post_data, timeout=self.TIMEOUT, headers=self.HEADERS)
        except requests.exceptions.Timeout as ex:
            return {"ok": False, "error": "Timeout"}

        # open("resplonse.html", "w").write(resp.text)

        if resp.status_code != 200:
            return {"ok": False, "error": f"Server responsed with code {resp.status_code}"}

        bs_page = BeautifulSoup(resp.text, "html.parser")
        open("post_response.html", "w").write(resp.text)

        try:
            full_info_href = bs_page.find(id="js_oTr0").td.a["href"]
            assert full_info_href is not None
        except TypeError:
            return {"ok": False, "error": "Invalid page recieved"}
        except AssertionError:
            return {"ok": False, "error": "Invalid page recieved"}
        except AttributeError:
            if "Не найдены данные, удовлетворяющие Вашему запросу" in resp.text:
                return {"ok": False, "error": "No data"}
            return {"ok": False, "error": "Invalid captcha"}

        full_info_href = full_info_href.replace("firLite®ion_key", "firLite&region_key")
        return {"ok": True, "full_info_href": full_info_href}

    def get_full_page(self, url):
        self.log("Requesting full info page")
        try:
            resp = self.SESSION.get(url, timeout=self.TIMEOUT + 10, headers=self.HEADERS)
        except requests.exceptions.Timeout:
            return {"ok": False, "error": "Timeout"}
        if resp.status_code != 200:
            return {"ok": False, "error": f"Server responsed with code {resp.status_code}"}
        open("full_info_doc.html", "w").write(resp.text)

        full_bs = BeautifulSoup(resp.text, "html.parser")
        all_block = full_bs.find_all('td', {'class': 'brdw1010'})

        lines = []
        for td_brd in all_block[:3]:
            [lines.append(line.strip()) for line in td_brd.find('table').text.split('\n')]

        str_list = list(filter(None, lines))

        b = [v for i, v in enumerate(str_list) if i % 2 == 0]
        a = [v for i, v in enumerate(str_list) if i % 2 == 1]
        result_json = dict(zip(b, a))

        all_block_ogr = (full_bs.find('div', {'id': 'r_enc'}))
        del all_block_ogr.attrs['style']

        rows_table2 = all_block_ogr.find("table").find_all("tr")
        if len(rows_table2) >= 3:
            td_right = rows_table2[2].find_all("td")[0]
            td_resr = rows_table2[2].find_all("td")[1]

            def parse_col(td_c, name):
                if td_c.find("table") is None:
                    result_json[name] = "".join(td_c.text).replace('  ', '').replace('\n', ' ')[1:]
                else:
                    res = ""
                    for i, row in enumerate(td_c.find("table").find_all("tr")):
                        td_n = row.find("td")
                        td_text = "".join(td_n.text).replace('  ', '').replace('\n', ' ')[1:]
                        res += td_text + "; "
                    result_json[name] = res

            parse_col(td_right, 'Право')
            parse_col(td_resr, 'Ограничение')

        result_json["request time"] = datetime.now().strftime("%Y-%m-%d/%H:%M:%S")
        # [lines_2.append(line.strip()) for line in all_block_ogr.find('table').text.split('\n')]
        # str_list_2 = list(filter(None, lines_2))
        # print(str_list_2)
        #
        # all_block_ogr.find('table')
        #
        # if len(str_list_2) == 5:
        #     result_json['Право'] = ' '.join(str_list_2[2:])
        #     result_json['Ограничение'] = ' '
        # if len(str_list_2) == 8:
        #     result_json['Право'] = ' '.join(str_list_2[2:5])
        #     result_json['Ограничение'] = ' '.join(str_list_2[5:8])
        # else:
        #     result_json['Право'] = ' '
        #     result_json['Ограничение'] = ' '

        # result = str(all_block)

        # print("RESULT json: ", result_json)

        open(f"{self.RESULT_FOLDER}/{result_json['Кадастровый номер:']}.json", "w").write(json.dumps(result_json))

        return {"ok": True}

    def run(self, cad_num, max_iter=20):
        self.log("Starting retrieving data")

        def get_captcha_text(iters):
            res_captcha = None
            while iters > 0:
                self.drop_session()
                res_captcha = self.get_form_page()
                if res_captcha["ok"]:
                    break
                self.log(str(res_captcha))
                iters -= 1
            return res_captcha, iters

        res_captcha, max_iter = get_captcha_text(max_iter)

        res_href = None
        while max_iter > 0:
            res_href = self.send_form(cad_num, res_captcha["captcha_text"])
            if res_href["ok"]:
                break
            elif res_href["error"] == "Invalid captcha":
                res_captcha, max_iter = get_captcha_text(max_iter)
            elif res_href["error"] == "No data":
                open(f"{self.RESULT_FOLDER}/{cad_num}.json", "w").write("""{"error": "No data"}""")
                max_iter = -1
                break
            self.log(str(res_href))
            max_iter -= 1

        result = None
        while max_iter > 0:
            result = self.get_full_page(self.FULL_PAGE_URL.format(res_href["full_info_href"]))
            if result["ok"]:
                break
            self.log(str(result))
            max_iter -= 1

        if result is not None and result["ok"]:
            self.log(f"Done retrieving information about {cad_num}")
        else:
            self.log(f"Max iteration number reached for {cad_num}")

    def test(self):
        res = self.get_form_page()
        if not res["ok"]:
            return res
        res = self.send_form("77:05:0002006:2499", res["captcha_text"])
        if not res["ok"]:
            return res
        return self.get_full_page(self.FULL_PAGE_URL.format(res["full_info_href"]))
