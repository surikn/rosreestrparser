from parser import RosReestrParser
from multiprocessing import Pool
from tqdm import tqdm
import sys

if __name__ == "__main__":
    if len(sys.argv) < 3:
        print("Usage: python run.py [path to txt file] [result folder]")
        sys.exit(0)

    try:
        cad_numbers = open(sys.argv[1]).read().split()
    except Exception as ex:
        print(f"Error when reading file: {ex}")

    parser = RosReestrParser(result_folder=sys.argv[2], solver_gpu=False, print_log=False)

    def run_parser(cad_num):
        done = False
        while not done:
            try:
                parser.run(cad_num)
                done = True
            except Exception as ex:
                print(f"Error: {ex}. Continuing...")
                done = False

    pool = Pool(10)

    for _ in tqdm(pool.imap_unordered(run_parser, cad_numbers), total=len(cad_numbers)):
        pass

    pool.terminate()
    pool.join()

