import requests
import uuid
import os
import sys
import time

folder = "train_captchas"


def save_image(pic_url, filename=None):
    if filename is None:
        filename = str(uuid.uuid4()) + ".jpg"
    path = f"{folder}/{filename}"

    print("Downloading captcha image")
    try:
        resp = requests.get(pic_url, timeout=5, stream=True)
    except requests.exceptions.Timeout as ex:
        return {"ok": False, "error": "Timeout"}
    if resp.status_code != 200:
        return {"ok": False, "error": f"Server responsed {resp.status_code}"}

    print("Saving image...")
    with open(path, 'wb') as handle:
        for block in resp.iter_content(1024):
            if not block:
                break
            handle.write(block)

    if os.path.getsize(path) == 0:
        os.remove(path)
        return {"ok": False, "error": "Invalid captcha image recieved"}

    return {"ok": True, "path": path, "filename": filename}


if __name__ == "__main__":
    if not os.path.exists(folder):
        os.makedirs(folder)

    iterations = int(sys.argv[1])

    image_url = "https://rosreestr.ru/wps/portal/p/cc_ib_portal_services/online_request/!ut/p/z1/04_Sj9CPykssy0xPLMnMz0vMAfIjo8zi3QNNXA2dTQy93QMNzQ0cPR29DY0N3Q0MQkz1w_Eq8DfUj6JEP1ABSL8BDuBoANQfhdcKZyMCCkBOJGRJQW5ohEGmpyIAKLXudw!!/p0/IZ7_01HA1A42KODT90AR30VLN22001=CZ6_GQ4E1C41KGQ170AIAK131G00T5=NJcaptcha=/?refresh=true&time={}"
    image_url = image_url.format(1585666409999)

    for i in range(iterations):
        image_url = image_url.format(int(time.time()))
        print(save_image(image_url))
        time.sleep(1)